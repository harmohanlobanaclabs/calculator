package com.example.samsung.calculator;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;

import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {
    Button b1, b2, b3, b4, b5;
    TextView t1, t2, t3;
    EditText e1, e2, e3;
    static double a,b,abc;
    String operator;
    ArrayList<String> list=new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        e1 = (EditText) findViewById(R.id.e1);
        e2 = (EditText) findViewById(R.id.e2);
        e3 = (EditText) findViewById(R.id.e3);
        b1 = (Button) findViewById(R.id.b1);
        b2 = (Button) findViewById(R.id.b2);
        b3 = (Button) findViewById(R.id.b3);
        b4 = (Button) findViewById(R.id.b4);
        b5 = (Button) findViewById(R.id.b5);
        t1 = (TextView) findViewById(R.id.t1);
        t2 = (TextView) findViewById(R.id.t2);
        t3 = (TextView) findViewById(R.id.t3);
    }

    public void add()
    {
        Double abc;
        abc=a+b;
        e3.setText(Double.toString(abc));
        list.add(a +"+" +b +"="+abc);
    }

    public void subtract()
    {
        Double abc;
        abc=a-b;
        if(a<b)
        {
            AlertDialog.Builder alerts = new AlertDialog.Builder(this);
            alerts.setTitle("Result is in negative because first value is less than the second");
            alerts.show();
        }
        e3.setText(Double.toString(abc));
        list.add(a +"-" +b +"="+abc);
    }

    public void multiply()
    {
        Double abc;
        abc=a*b;
        e3.setText(Double.toString(abc));
        list.add(a +"*" +b +"="+abc);
     }
    public void change()
    {
        e1.setText(null);
        e2.setText(null);
    }

    public void divide()
    {
        Double abc;
        abc=a/b;
        if(b==0)
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Denominator can not be zero !");
            dialog.show();
            change();
        }
        e3.setText(Double.toString(abc));
        list.add(a +"/" +b +"="+abc);
     }

    public void summary()
    {
        Intent intent = new Intent(this,Summary.class);
        intent.putExtra("data",list);
        startActivity(intent);
    }

    public void onClick(View view) {
        int id = view.getId();
        if (view.getId() == R.id.b5)
        {
            summary();
        }
        String a1 = e1.getText().toString();
        String a2 = e2.getText().toString();
        if (a1.isEmpty() || a2.isEmpty())
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("Please fill the details !");
            dialog.show();
        } else
        {
            a = Double.parseDouble(e1.getText().toString());
            b = Double.parseDouble(e2.getText().toString());

                switch (id) {
                    case R.id.b1:
                        add();
                        break;
                    case R.id.b2:
                        subtract();
                        break;
                    case R.id.b3:
                        multiply();
                        break;
                    case R.id.b4:
                        divide();
                        break;
                    default:
                        break;
                }
            }
        }
    }


