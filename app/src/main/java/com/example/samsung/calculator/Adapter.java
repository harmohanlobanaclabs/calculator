package com.example.samsung.calculator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.samsung.calculator.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by samsung on 2/1/2015.
 */
public class Adapter extends BaseAdapter {
    public List<String> list;
    Context ab;

    public Adapter(ArrayList<String>list,Context ab)
    {
        this.ab=ab;
        this.list=list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater=(LayoutInflater)ab.getSystemService(ab.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row, null);
        TextView tv = (TextView)view.findViewById(R.id.text_view);
        tv.setText(list.get(position));
        return view;
    }
}
